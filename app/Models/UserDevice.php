<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class UserDevice extends Model
{

    use Notifiable;

    protected  $guarded = [];

    public function routeNotificationForOneSignal()
    {
        return $this->device_id;
    }
}
