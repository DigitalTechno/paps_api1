<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBudgetItem extends Model
{
    protected $guarded = [];
}
