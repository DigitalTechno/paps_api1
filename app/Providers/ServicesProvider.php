<?php

namespace App\Providers;

use App\Services\Math\MathService;
use App\Services\Math\MathServiceInterface;
use Illuminate\Support\ServiceProvider;

class ServicesProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(MathServiceInterface::class , MathService::class);
    }
}
