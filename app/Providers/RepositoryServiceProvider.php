<?php

namespace App\Providers;

use App\Repositories\Advert\AdvertInterface;
use App\Repositories\Advert\AdvertRepository;
use App\Repositories\AdvertCategory\AdvertCategoryInterface;
use App\Repositories\AdvertCategory\AdvertCategoryRepository;
use App\Repositories\AdvertGroup\AdvertGroupInterface;
use App\Repositories\AdvertGroup\AdvertGroupRepository;
use App\Repositories\BudgetItem\BudgetItemInterface;
use App\Repositories\BudgetItem\BudgetItemRepository;
use App\Repositories\Document\DocumentInterface;
use App\Repositories\Document\DocumentRepository;
use App\Repositories\FixedBudgetItem\FixedBudgetItemInterface;
use App\Repositories\FixedBudgetItem\FixedBudgetItemRepository;
use App\Repositories\PasswordReset\PasswordResetInterface;
use App\Repositories\PasswordReset\PasswordResetRepository;
use App\Repositories\Notification\NotificationRepository;
use App\Repositories\Notification\NotificationRepositoryInterface;
use App\Repositories\User\UserInterface;
use App\Repositories\User\UserRepository;
use App\Repositories\UserBalance\UserBalanceInterface;
use App\Repositories\UserBalance\UserBalanceRepository;
use App\Repositories\UserBudgetItem\UserBudgetItemInterface;
use App\Repositories\UserBudgetItem\UserBudgetItemRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(FixedBudgetItemInterface::class , FixedBudgetItemRepository::class);
        $this->app->bind(BudgetItemInterface::class  , BudgetItemRepository::class);
        $this->app->bind(UserBudgetItemInterface::class, UserBudgetItemRepository::class);
        $this->app->bind(UserBalanceInterface::class , UserBalanceRepository::class);

        $this->app->bind(AdvertInterface::class , AdvertRepository::class);
        $this->app->bind(AdvertCategoryInterface::class , AdvertCategoryRepository::class);
        $this->app->bind(AdvertGroupInterface::class , AdvertGroupRepository::class);

        $this->app->bind(DocumentInterface::class , DocumentRepository::class);

        $this->app->bind(UserInterface::class , UserRepository::class);

        $this->app->bind(PasswordResetInterface::class , PasswordResetRepository::class);
        $this->app->bind(NotificationRepositoryInterface::class, NotificationRepository::class);
    }
}
