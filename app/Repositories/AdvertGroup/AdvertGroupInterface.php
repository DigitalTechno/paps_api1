<?php

namespace  App\Repositories\AdvertGroup;

interface AdvertGroupInterface{

    public function getAdvertGroup(string $type);

}