<?php

namespace App\Repositories\AdvertGroup;

use App\Models\AdvertGroup;

class AdvertGroupRepository implements AdvertGroupInterface
{
    private $model;

    public function __construct(AdvertGroup $advertGroup)
    {
        $this->model = $advertGroup;
    }

    public function getAdvertGroup(string $type)
    {
        return $this->model->where('type' , $type)->first();
    }
}