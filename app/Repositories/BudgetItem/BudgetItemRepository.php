<?php

namespace App\Repositories\BudgetItem;

use App\Models\BudgetItem;
use App\User;
use Carbon\Carbon;

class BudgetItemRepository implements BudgetItemInterface
{

    private $model;

    private $fromDate;

    private $toDate;

    public function __construct(BudgetItem $budgetItem){
        $this->model = $budgetItem;
    }

    /**
     * @param User $user
     * @param int $week
     * @param bool $include_all_items
     * @return mixed
     */
    public function getUserIncomeItems(User $user)
    {
       return $this->getUserBudgetItems($user , 'income');
    }

    /**
     * @param User $user
     * @param int $week
     * @param bool $include_all_items
     * @return mixed
     */
    public function getUserExpenseItems(User $user)
    {
        return $this->getUserBudgetItems($user , 'expense');
    }


    private function getUserBudgetItems(User $user , string $type){

        $query = $this->model
            ->select('budget_items.name' , 'budget_items.type' , 'budget_items.fixed' , 'user_budget_items.*' )
            ->join('user_budget_items' , 'user_budget_items.budget_item_id' , '=' , 'budget_items.id')
            ->where('user_budget_items.user_id' , $user->id)
            ->where('budget_items.type' , $type)
            ->whereBetween('date', $this->getRange());


        return $query->get();


    }

    /**
     * @return array
     */
    private function getRange(){

        $this->fromDate = Carbon::now()->subMonth()->startOfMonth()->toDateString();
        $this->toDate = Carbon::now()->startOfMonth()->toDateString();

        return [$this->fromDate , $this->toDate];
    }


    public function getFixedExpenseItems()
    {
        return $this->model
            ->where('fixed' , 1)
            ->where('type' , 'expense')
            ->get();
    }
}