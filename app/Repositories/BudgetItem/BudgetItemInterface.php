<?php

namespace App\Repositories\BudgetItem;

use App\User;

interface BudgetItemInterface{

    public function getUserIncomeItems(User $user);

    public function getUserExpenseItems(User $user);

    public function getFixedExpenseItems();

}