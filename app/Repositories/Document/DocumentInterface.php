<?php

namespace App\Repositories\Document;

use App\Models\File;
use App\User;
use Illuminate\Http\Request;

interface DocumentInterface
{
    /**
     * @param User $user
     * @return mixed
     */
    public function getDocuments(User $user);

    /**
     * @param User $user
     * @return mixed
     */
    public function getUserDocuments(User $user);

    /**
     * @param User $user
     * @param Request $request
     */
    public function addUserDocument(User $user, Request $request);

    /**
     * @param int $user_document_id
     * @return mixed
     */
    public function getUserDocument(int $user_document_id);

    /**
     * @param int $document_id
     * @return mixed
     */
    public function getDocument(int $document_id);

    /**
     * @param Request $request
     */
    public function updateUserDocument(Request $request);

    /**
     * @param int $document_id
     * @return mixed
     */
    public function delete(int $document_id);

    /**
     * @param int $file_id
     * @return mixed
     */
    public function deleteFile(int $file_id);

    /**
     * @param Request $request
     * @param User $user
     */
    public function addDocumentFile(Request $request, User $user);

}