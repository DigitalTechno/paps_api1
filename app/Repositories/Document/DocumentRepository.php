<?php

namespace App\Repositories\Document;

use DB;
use App\Models\Document;
use App\Models\File;
use App\User;
use App\Models\UserDocument;
use App\Models\UserDocumentFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DocumentRepository implements DocumentInterface
{
    private $model;
    private $userDocument;

    public function __construct(
        Document $model,
        UserDocument $userDocument
    )
    {
        $this->model = $model;
        $this->userDocument = $userDocument;
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function getDocuments(User $user)
    {
        $uploadedDocumentIds =$this->userDocument->select('document_id')
            ->where('user_id', $user->id)
            ->pluck('document_id')
            ->toArray();

        return $this->model->whereNotIn('id', $uploadedDocumentIds)->where('archived', 0)->get();
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function getUserDocuments(User $user)
    {
        $user_documents = $this->userDocument->select('user_documents.*',
            'documents.title',
            'documents.expires',
            DB::raw('(CASE
                WHEN (CURRENT_DATE() > DATE(user_documents.expiry_date)) THEN 1 
                ELSE 0                
                END) as expired'))
            ->join('documents', 'documents.id', '=', 'user_documents.document_id')
            ->where('user_documents.user_id', $user->id)
            ->get();

        foreach ($user_documents as &$user_document){
            $user_document->files = $this->getUserDocumentFiles($user_document->id);
        }
        return $user_documents;
    }

    public function getUserDocumentFiles($user_document_id){
        return UserDocumentFile::select('files.url', 'files.id')
            ->join('files', 'files.id', '=', 'user_document_files.file_id')
            ->where('user_document_files.user_document_id', $user_document_id)
            ->get();
    }

    /**
     * @param User $user
     * @param Request $request
     */
    public function addUserDocument(User $user, Request $request)
    {
        $document = new $this->userDocument;
        $document->user_id = $user->id;
        $document->document_id = $request->document_id;
        $document->expiry_date = $request->expiry_date;
        $document->save();
        return $document;
    }

    /**
     * @param int $user_document_id
     * @return mixed
     */
    public function getUserDocument(int $user_document_id)
    {
        $user_document = $this->userDocument->select('user_documents.*',
            'documents.title',
            'documents.expires')
            ->join('documents', 'documents.id', '=', 'user_documents.document_id')
            ->where('user_documents.id', $user_document_id)
            ->first();
        $user_document->files = $this->getUserDocumentFiles($user_document_id);
        return $user_document;
    }

    /**
     * @param int $document_id
     * @return mixed
     */
    public function getDocument(int $document_id)
    {
        return $this->model->find($document_id);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function updateUserDocument(Request $request)
    {
        $document = $this->userDocument->find($request->document_id);
        $document->expiry_date = $request->expiry_date;
        $document->save();
        return $document;
    }

    /**
     * @param int $document_id
     *
     */
    public function delete(int $document_id)
    {
        $documents = $this->userDocument->where('document_id', $document_id)->get();
        foreach ($documents as $document) {
            $document = $this->userDocument->find($document->id);
            $file = File::where('id', $document->file_id)->first();
            unlink(public_path('storage/' . $file->path));
            $document->delete();
            $file->delete();
        }
    }

    public function updateDocumentFile(Request $request, User $user)
    {
        $document = $this->updateUserDocument($request);
        $dir = "documents/" . $user->id;
        $uploadedFiles = $request->uploadedFiles;
        $this->uploadDocumentFile($uploadedFiles, $dir, $document);
    }

    public function addDocumentFile(Request $request, User $user)
    {
        $user_document = $this->addUserDocument($user, $request);
        $dir = "documents/" . $user->id;
        $uploadedFiles = $request->uploadedFiles;
        $this->uploadDocumentFile($uploadedFiles, $dir, $user_document);
    }

    public function addUserDocumentFile($file, $user_document)
    {
        $user_document_file = new UserDocumentFile();
        $user_document_file->file_id = $file->id;
        $user_document_file->user_document_id = $user_document->id;
        $user_document_file->save();
    }

    public function deleteFile(int $file_id)
    {
        $user_documents = 0;
        $documentFile = UserDocumentFile::where('file_id', $file_id)->first();
        $user_document_files = UserDocumentFile::where('user_document_id', $documentFile->user_document_id)->count();
        $file = File::where('id', $file_id)->first();
        unlink(public_path('storage/' . $file->path));
        UserDocumentFile::where('file_id', $file_id)->delete();
        $file->delete();
        if($user_document_files == 1){
            $user_documents = 1;
            $this->userDocument->where('id', $documentFile->user_document_id)->delete();
        }
        return $user_documents;
    }

    public function uploadDocumentFile($uploadedFiles, $dir, $user_document)
    {
        if($uploadedFiles) {
            foreach ($uploadedFiles as $uFile) {
                if ($uFile['file']) {
                    $name = $uFile['fileName'];
                    $fileType = $uFile['fileType'];
                    $fileExt = $uFile['fileExt'];
                    $fileSize = $uFile['fileSize'];
                    $fileBase64 =  $uFile['file'];
                    $image = str_replace('data:'.$fileType . ';base64,', '', $fileBase64);
                    $fileName = str_random(64) . '.' . $fileExt;
                    $path = $dir . '/' . $fileName;
                    $img = base64_decode($image);
                    Storage::disk('public')->put($path, $img);

                    $file = new File();
                    $file->name = $name;
                    $file->path = $path;
                    $file->disk = 'public';
                    $file->url = Storage::disk('public')->url($path);;
                    $file->extension = $fileExt;
                    $file->mime = $fileType;
                    $file->size = $fileSize;
                    $file->save();

                    $this->addUserDocumentFile($file, $user_document);
                }
            }
        }
    }


}