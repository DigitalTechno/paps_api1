<?php

namespace App\Repositories\UserBalance;


use App\Repositories\UserBudgetItem\UserBudgetItemInterface;
use App\Services\Math\MathServiceInterface;
use App\User;
use App\Models\UserBalance;
use Carbon\Carbon;


class UserBalanceRepository implements UserBalanceInterface
{

    private $model;

    private $userBudgetItemInterface;

    private $mathServiceInterface;

    public function __construct(
        UserBalance $userBalance,
        UserBudgetItemInterface $userBudgetItemInterface,
        MathServiceInterface $mathServiceInterface
    ){
        $this->model = $userBalance;
        $this->userBudgetItemInterface = $userBudgetItemInterface;
        $this->mathServiceInterface = $mathServiceInterface;
    }


    /**
     * @param User $user
     * @return mixed
     */
    public function getUserBalance(User $user)
    {

        $balance_of_income_and_expense = $this->userBudgetItemInterface
            ->getUserTotalBudget($user);

        $net_income = $this->getNetIncome($user)->net_income ?? 0;

        $balance = $this->mathServiceInterface
            ->calculateBalance($net_income , $balance_of_income_and_expense);

        //  update balance if user does not exist for some reason record will get created
        $user_balance = $this->updateBalance($user , $balance);

        if($user_balance->balance == null){
            return 0.00;
        }

        return $user_balance->balance;

    }

    /**
     * @param User $user
     * @param float $balance
     * @return mixed
     */
    public function updateBalance(User $user , float $balance)
    {
        $date = Carbon::now()->lastOfMonth()->toDateString();

        return $this->model
            ->updateOrCreate([
                'user_id' => $user->id,
                'date' => $date
            ],
            [
                'balance' => $balance
            ]);
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function getUserPayslips(User $user)
    {

        //  go four months back
        $fromDate = Carbon::now()->subMonth(4)->lastOfMonth()->toDateString();
        $toDate = Carbon::now()->lastOfMonth()->toDateString();

        return $this->model->where('user_id' , $user->id)
            ->whereBetween('date' , [$fromDate , $toDate])
            ->whereNotNull('payslip_url')
            ->orderBy('date' , 'DESC')
            ->get();

    }

    /**
     * @param User $user
     * @return mixed
     */
    public function getNetIncome(User $user)
    {
        $date = Carbon::now()->lastOfMonth()->toDateString();

        return $this->model->where('user_id' , $user->id)
            ->where('date' , $date)
           ->first();
    }
}