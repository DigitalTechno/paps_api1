<?php

namespace App\Repositories\UserBalance;

use App\User;

interface UserBalanceInterface{

    /**
     * @param User $user
     * @return mixed
     */
    public function getUserBalance(User $user);

    /**
     * @param User $user
     * @param float $balance
     * @return mixed
     */
    public function updateBalance(User $user , float  $balance);


    public function getUserPayslips(User $user);

    /**
     * @param User $user
     * @return mixed
     */
    public function getNetIncome(User $user);
}