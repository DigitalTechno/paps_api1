<?php

namespace App\Repositories\Advert;

use App\Models\AdvertCategory;
use App\User;

interface AdvertInterface {

    public function getAdvertsForCategory(AdvertCategory $advertCategory , User $user);


}