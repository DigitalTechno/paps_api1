<?php

namespace App\Repositories\Advert;

use App\Models\Advert;
use App\Models\AdvertCategory;
use App\Repositories\AdvertGroup\AdvertGroupInterface;
use App\Repositories\UserBalance\UserBalanceInterface;
use App\User;
use Illuminate\Support\Facades\DB;

class AdvertRepository implements AdvertInterface
{

    private $model;

    private $advertGroupInterface;

    private $userBalanceInterface;

    public function __construct(
        Advert $advert,
        AdvertGroupInterface $advertGroupInterface,
        UserBalanceInterface $userBalanceInterface

    )
    {
        $this->model = $advert;
        $this->advertGroupInterface = $advertGroupInterface;
        $this->userBalanceInterface = $userBalanceInterface;
    }


    public function getAdvertsForCategory(AdvertCategory $advertCategory, User $user)
    {

        //  get balance
        $balance = $this->userBalanceInterface
            ->getUserBalance($user); // consider making balance a private property

        $type = $balance < 0 ? 'negative' : 'positive';

        $advert_group = $this->advertGroupInterface->getAdvertGroup($type);

        return $this->model->select('adverts.*', 'files.url')
            ->where('adverts.advert_category_id' , $advertCategory->id)
            ->join('files', 'files.id', '=', 'adverts.file_id')
            ->whereIn('adverts.advert_group_id' , [$advert_group->id ,1])
            ->where('expiry_date' , '>=' , DB::raw('curdate()'))
            ->get();
    }
}