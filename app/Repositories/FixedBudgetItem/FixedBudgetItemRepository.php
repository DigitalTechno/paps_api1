<?php

namespace App\Repositories\FixedBudgetItem;

use App\Models\FixedBudgetItem;
use App\User;
use App\Models\UserFixedBudgetItem;

class FixedBudgetItemRepository implements FixedBudgetItemInterface
{
    private $model;

    public function __construct(FixedBudgetItem $fixedBudgetItem)
    {
        $this->model = $fixedBudgetItem;
    }

    public function getUserFixedBudgetItems(User $user)
    {

        $query = $this->model->leftJoin(
            'user_fixed_budget_items' ,
            'user_fixed_budget_items.fixed_budget_item_id' ,
            '=' ,
            'fixed_budget_items.id'
        );


        //  check if user has items
        if($this->userHasFixedItemsSet($user)){
            $query->where('user_id', $user->id)
                ->where('active' , 1);
        }


        return $query->get();

    }

    private function userHasFixedItemsSet(User $user){

        //  this might need refactoring
        $items = UserFixedBudgetItem::where('user_id' , $user->id)->get();

        if($items->count()){
            return true;
        }

        return false;

    }
}