<?php

namespace  App\Repositories\FixedBudgetItem;

use App\User;

interface FixedBudgetItemInterface{

    public function getUserFixedBudgetItems(User $user);
}