<?php

namespace App\Repositories\Notification;

use App\User;
use App\Models\Notification;
use App\Models\UserNotification;

class NotificationRepository implements NotificationRepositoryInterface
{
    protected $notification;
    protected $userNotification;

    public function __construct(
        Notification $notification,
        UserNotification $userNotification
    )
    {
        $this->notification = $notification;
        $this->userNotification = $userNotification;
    }

    public function all(User $user)
    {
        $notifications = $this->notification->select('id', 'title')
            ->where('status', 1)
            ->orderBy('notifications.sent_at', 'desc')
            ->get();

        foreach ($notifications as &$notification)
        {
            $user_notification = $this->userNotification::where(['notification_id' => $notification->id, 'user_id' => $user->id])->first();
            $notification->read = isset($user_notification) ? $user_notification->read : 0;
        }

        return $notifications;
    }

    public function getNotification(int $notificationId, User $user)
    {
        $user_notification = $this->userNotification::where(['notification_id' => $notificationId, 'user_id' => $user->id])->first();
        $notification = $this->notification::where('notifications.id', $notificationId)->first();
        $notification->read = isset($user_notification) ? $user_notification->read : 0;
        return $notification;
    }

    public function getLatestNotifications(User $user)
    {
        $notifications = $this->notification::select( 'notifications.id', 'notifications.title')
            ->where('status', 1)
            ->orderBy('notifications.sent_at', 'desc')
            ->limit(5)
            ->get();

        foreach ($notifications as &$notification)
        {
            $user_notification = $this->userNotification::where(['notification_id' => $notification->id, 'user_id' => $user->id])->first();
            $notification->read = isset($user_notification) ? $user_notification->read : 0;
        }

        return $notifications;
    }

    public function getUnreadNotifications(User $user)
    {
        $user_notifications = $this->userNotification->where(['user_id' => $user->id, 'read' => 1])
            ->pluck('notification_id')
            ->toArray();

        return $this->notification->whereNotIn('id', $user_notifications)
            ->where('status', 1)
            ->count();
    }

    public function markNotificationAsRead(int $notificationId, User $user)
    {
        $notification = new $this->userNotification;
        $notification->user_id = $user->id;
        $notification->notification_id = $notificationId;
        $notification->read = 1;
        $notification->save();
    }
}
