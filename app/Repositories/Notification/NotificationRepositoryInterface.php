<?php

namespace App\Repositories\Notification;

use App\Models\Notification;
use App\User;

interface NotificationRepositoryInterface
{
    /**
     * Gets all Notifications.
     *
     * @return array Notification
     * @param User $user
     */
    public function all(User $user);

    /**
     * Gets a Notification by its ID
     *
     * @param int $notificationId
     * @param User $user
     *
     * @return Notification
     */
    public function getNotification(int $notificationId, User $user);

    /**
     * get unread Notifications.
     *
     * @param User $user
     */
    public function getUnreadNotifications(User $user);

    /**
     * mark Notification as read.
     *
     * @param int $notificationId
     * @param User $user
     */
    public function markNotificationAsRead(int $notificationId, User $user);

    /**
     * get latest Notifications.
     *
     * @param User $user
     */
    public function getLatestNotifications(User $user);

}
