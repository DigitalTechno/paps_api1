<?php

namespace App\Repositories\UserBudgetItem;

use App\User;
use Illuminate\Http\Request;

interface UserBudgetItemInterface{

    public function assignDefaultExpensesToUser(User $user);

    public function getUserTotalBudget(User $user);

}