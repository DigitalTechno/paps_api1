<?php

namespace App\Repositories\UserBudgetItem;

use App\Repositories\BudgetItem\BudgetItemInterface;
use App\User;
use App\Models\UserBudgetItem;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class UserBudgetItemRepository implements UserBudgetItemInterface
{

    private $model;

    private $budgetItemInterface;

    public function __construct(UserBudgetItem $userBudgetItem , BudgetItemInterface $budgetItemInterface)
    {
        $this->model = $userBudgetItem;
        $this->budgetItemInterface = $budgetItemInterface;
    }

    public function assignDefaultExpensesToUser(User $user)
    {

        //  get fixed items
        $budget_items = $this->budgetItemInterface->getFixedExpenseItems();

        foreach ($budget_items as $item){
            $this->model->create([
                'user_id' => $user->id,
                'budget_item_id' => $item->id,
                'week' => 1,
                'date' => Carbon::now()->startOfMonth()
            ]);
        }


    }

    public function getUserTotalBudget(User $user)
    {

        $fromDate = Carbon::now()->subMonth()->startOfMonth()->toDateString();
        $toDate = Carbon::now()->startOfMonth()->toDateString();
        $range = [$fromDate , $toDate];

       $data = $this->model
           ->select(DB::raw('SUM(amount) as balance'))
           ->where('active' , 1)
           ->where('user_id' , $user->id)
           ->whereBetween('date' , $range)
            ->first();

       return $data->balance;
    }
}