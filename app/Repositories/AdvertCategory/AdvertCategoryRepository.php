<?php

namespace App\Repositories\AdvertCategory;

use App\Models\AdvertCategory;
use App\Repositories\Advert\AdvertInterface;
use App\Repositories\AdvertGroup\AdvertGroupInterface;
use App\Repositories\UserBalance\UserBalanceInterface;
use App\User;
use Illuminate\Support\Facades\DB;

class AdvertCategoryRepository implements AdvertCategoryInterface
{

    private $model;

    private $userBalanceInterface;

    private $advertGroupInterface;

    private $advertInterface;

    public function __construct(
        AdvertCategory $advertCategory ,
        UserBalanceInterface $userBalanceInterface,
        AdvertInterface $advertInterface,
        AdvertGroupInterface $advertGroupInterface
    ){
        $this->model = $advertCategory;
        $this->userBalanceInterface = $userBalanceInterface;
        $this->advertInterface = $advertInterface;
        $this->advertGroupInterface = $advertGroupInterface;
    }

    public function getCategoriesForUser(User $user)
    {
        //  get balance
        $balance = $this->userBalanceInterface
            ->getUserBalance($user); // consider making balance a private property

        $type = $balance < 0 ? 'negative' : 'positive';


        $advert_group = $this->advertGroupInterface->getAdvertGroup($type);

        return $this->model->select('advert_categories.*')
            ->join('adverts' , 'adverts.advert_category_id' , '=' , 'advert_categories.id')
            ->whereIn('adverts.advert_group_id' , [$advert_group->id , 1])
            ->where('adverts.expiry_date' , '>=' , DB::raw('curdate()'))
            ->groupBy('advert_categories.id')
            ->get();

    }


}