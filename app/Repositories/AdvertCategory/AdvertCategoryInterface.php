<?php

namespace App\Repositories\AdvertCategory;

use App\User;

interface AdvertCategoryInterface{

    public function getCategoriesForUser(User $user);

}