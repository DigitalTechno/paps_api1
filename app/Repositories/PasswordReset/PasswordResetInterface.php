<?php

namespace App\Repositories\PasswordReset;

interface PasswordResetInterface {
    public function add();
}