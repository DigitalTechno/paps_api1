<?php

namespace App\Repositories\PasswordReset;

use App\Models\PasswordReset;
use Carbon\Carbon;

class PasswordResetRepository implements PasswordResetInterface
{

    private $model;

    public function __construct(PasswordReset $passwordReset)
    {
        $this->model = $passwordReset;
    }


    public function add()
    {
        //  password reset links expire in 4 hours
        return $this->model->create([
            'token' => str_random(60),
            'expiry_date' => Carbon::now()->addMinutes(15)->toDateTimeString()
        ]);
    }
}