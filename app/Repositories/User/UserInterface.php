<?php

namespace App\Repositories\User;

use App\User;

interface UserInterface{


    public function getClients();

    public function createAuthenticatedToken(User $user);

}