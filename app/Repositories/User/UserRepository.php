<?php

namespace App\Repositories\User;

use App\User;
use Tymon\JWTAuth\Facades\JWTAuth;


class UserRepository implements UserInterface
{

    private $model;

    public function __construct(User $user){
        $this->model = $user;
    }

    public function getClients()
    {
        return $this->model->where('user_type_id' , 1)->get();
    }

    public function createAuthenticatedToken(User $user)
    {
        $token = JWTAuth::fromUser($user);
        return  $token;
    }
}