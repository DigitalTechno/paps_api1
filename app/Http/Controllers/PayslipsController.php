<?php

namespace App\Http\Controllers;

use App\Repositories\UserBalance\UserBalanceInterface;
use Illuminate\Http\Request;

class PayslipsController extends Controller
{

    private $userBalanceInterface;

    public function __construct(UserBalanceInterface $userBalanceInterface){

        $this->middleware('auth:api');
        $this->userBalanceInterface = $userBalanceInterface;

    }

    public function getPayslips(){

        return $this->userBalanceInterface->getUserPayslips(auth()->user());

    }
}
