<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api' , ['except' => ['login']]);
    }

    public function login(Request $request){

        //  account must be active to login.
        $request->request->add(['active' => 1]);

        $credentials = request(['id_number', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['message' => 'Invalid username or password.'], 401);
        }

        //  at this point user is authenticated - user object available
        if(auth()->user()->active == 0){
            return response()->json(['message' => config('errors.inactive_account') ] , 401);
        }

        return $this->respondWithToken($token);

    }

    public function me(){

       return \auth()->user();
    }

    protected function respondWithToken($token){
        return response()->json([
            'token' => $token,
            'user' => auth()->user(),
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
