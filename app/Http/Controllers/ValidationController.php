<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ValidationController extends Controller
{

    public function idNumberExists(Request $request){
        $idNumberExists = false;
        $status_code = 200;

        $validator = Validator::make($request->all() , [
            'id_number' => 'unique:users'
        ]);


        if($validator->fails()){
            $idNumberExists = true;
            $status_code = 400;
        }

        return response()->json([
            'idNumberExists' => $idNumberExists
        ] , $status_code);
    }


}
