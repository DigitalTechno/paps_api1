<?php

namespace App\Http\Controllers;

use App\Repositories\Notification\NotificationRepositoryInterface;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    protected $notificationRepository;

    public function __construct(NotificationRepositoryInterface $notificationRepository)
    {
        $this->middleware('auth:api');
        $this->notificationRepository = $notificationRepository;
    }

    public function getNotifications()
    {
        return $this->notificationRepository->all(auth()->user());
    }

    public function getNotification($notificationId)
    {
        return $this->notificationRepository->getNotification($notificationId, auth()->user());
    }

    public function markNotificationAsRead($notificationId)
    {
        return $this->notificationRepository->markNotificationAsRead($notificationId, auth()->user());
    }

    public function getUnreadNotifications()
    {
        return $this->notificationRepository->getUnreadNotifications(auth()->user());
    }

    public function getLatestNotifications()
    {
        return $this->notificationRepository->getLatestNotifications(auth()->user());
    }
}
