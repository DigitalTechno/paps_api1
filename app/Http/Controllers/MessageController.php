<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Artisan;
use Mail;
use Config;
use App\User;
use App\Mail\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    private $message;

    public function __construct(\App\Message $message)
    {
        $this->middleware('auth:api');
        $this->message = $message;
    }
    public function send(Request $request)
    {
        $user = auth()->user();

        $message = new $this->message();
        $message->user_id = $user->id;
        $message->message = $request->message;
        $message->save();

        $toEmail = 'hrs@phangela.co.za';
        $admin = User::where('user_type_id', 1)->first();
        if($admin){
            $toEmail = $admin->hr_email ? $admin->hr_email : $toEmail;
        }

        Mail::to($toEmail)->queue(new Message($user, $message));
    }

}
