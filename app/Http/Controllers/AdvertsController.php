<?php

namespace App\Http\Controllers;

use App\AdvertCategory;
use App\Repositories\Advert\AdvertInterface;
use Illuminate\Http\Request;

class AdvertsController extends Controller
{


    private $advertInterface;

    public function __construct(AdvertInterface $advertInterface)
    {
        $this->middleware('auth:api');
        $this->advertInterface = $advertInterface;

    }


    public function getAdvertsForCategory($id){

        try{

            $advert_category = AdvertCategory::find($id);

            return $this->advertInterface
                ->getAdvertsForCategory($advert_category , auth()->user());

        }
        catch (\Exception $e){

            return response()->json([
                'error'=> $e->getMessage(),
                'message' => config('errors.general')
            ] , 500);

        }


    }


}
