<?php

namespace App\Http\Controllers;

use App\Repositories\AdvertCategory\AdvertCategoryInterface;
use Illuminate\Http\Request;

class AdvertCategoriesController extends Controller
{

    private $advertCategoryInterface;

    public function __construct(
        AdvertCategoryInterface $advertCategoryInterface
    )
    {
        $this->middleware('auth:api');
        $this->advertCategoryInterface = $advertCategoryInterface;
    }



    public function getCategoriesForUser(){

        try{
            return $this->advertCategoryInterface->getCategoriesForUser(auth()->user());
        }
        catch (\Exception $e){

            return response()->json([
                'error' => $e->getMessage(),
                'message' => config('errors.general')
            ] , 500);
        }



    }



}
