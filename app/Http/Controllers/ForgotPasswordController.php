<?php

namespace App\Http\Controllers;

use App\Notifications\PasswordReset;
use App\Repositories\PasswordReset\PasswordResetInterface;
use App\User;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{

    private $passwordResetInterface;

    public function __construct(PasswordResetInterface $passwordResetInterface){
        $this->passwordResetInterface = $passwordResetInterface;
    }

    public function sendPasswordResetSMS(Request $request){

        try{

            $user = User::where('id_number' , $request->id_number)->first();

            if($user != null){

                $password_reset = $this->passwordResetInterface->add();

                $token = $password_reset->token;

                $user->notify(new PasswordReset($token));

                return response()->json([
                    'message' => 'An SMS with a password reset link will be sent to you shortly.'
                ] , 200);
            }

            return response()->json([
                'message' => 'Unable to authenticate using the ID number provided.'
            ] , 401);

        }
        catch(\Exception $e){

            return response()->json([
                'message' => config('errors.general'),
                'error' => $e->getMessage()
            ] , 500);
        }

    }

}
