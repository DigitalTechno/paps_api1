<?php

namespace App\Http\Controllers;

use App\Models\BudgetItem;
use App\Repositories\BudgetItem\BudgetItemInterface;
use App\Repositories\UserBudgetItem\UserBudgetItemInterface;
use App\Services\Math\MathServiceInterface;
use App\User;
use App\Models\UserBalance;
use App\Models\UserBudgetItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BudgetController extends Controller
{

    private $fixedBudgetItemInterface;

    private $budgetItemInterface;

    private $mathServiceInterface;

    private $userBudgetItemInterface;

    public function __construct(
        BudgetItemInterface $budgetItemInterface,
        MathServiceInterface $mathServiceInterface,
        UserBudgetItemInterface $userBudgetItemInterface

    )
    {
        $this->middleware('auth:api');
        $this->budgetItemInterface = $budgetItemInterface;
        $this->mathServiceInterface = $mathServiceInterface;
        $this->userBudgetItemInterface = $userBudgetItemInterface;



    }

    public function getUserFixedItems($user_id){

        $user = User::find($user_id);

        return $this->fixedBudgetItemInterface
            ->getUserFixedBudgetItems($user);

    }

    public function addItem(Request $request){

        DB::beginTransaction();

        try{

            $date = Carbon::now()
                ->startOfMonth()
                ->toDateTimeString();

            $budget_item = BudgetItem::updateOrCreate(
                ['id' => $request->budget_item_id],
                [
                    'name' => $request->name,
                    'type' => $request->type,
                    'fixed' => $request->fixed
                ]
            );

            if($request->type == 'income'){
                $amount = $request->amount;
            }
            else{
                $amount = $request->amount == null ? $request->amount : $this->mathServiceInterface
                    ->transformToNegative((float)$request->amount);
            }


            $user_budget_item = UserBudgetItem::updateOrCreate(
                ['id' => $request->user_budget_item_id],
                [
                    'user_id' => auth()->user()->id,
                    'budget_item_id' => $budget_item->id,
                    'amount' => $amount,
                    'week' => $request->week,
                    'date' => $date
                ]
            );

            DB::commit();

            return response()->json([
                'user_budget_item' => $user_budget_item
            ]);

        }
        catch (\Exception $e){
            DB::rollBack();

            return response()->json([
                'error' => $e->getMessage(),
                'message' => config('errors.general')
            ] , 500);
        }

    }

    public function getIncomeItems(){

        return $this->budgetItemInterface
            ->getUserIncomeItems(auth()->user());
    }

    public function getExpenseItems(){

        return $this->budgetItemInterface
            ->getUserExpenseItems(auth()->user());
    }

    public function getUserBudgetItems($id){
        $user = User::find($id);

        return $this->budgetItemInterface->getUserBudgetItem($user);
    }

    public function removeItem($id){
       return UserBudgetItem::where('id' , $id)->delete();
    }



}
