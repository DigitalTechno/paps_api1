<?php

namespace App\Http\Controllers;

use App\Models\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Repositories\Document\DocumentInterface;

class DocumentController extends Controller
{
    private $documentInterface;

    public function __construct(DocumentInterface $documentInterface)
    {
        $this->middleware('auth:api');
        $this->documentInterface = $documentInterface;
    }

    public function getDocuments()
    {
        return $this->documentInterface->getDocuments(auth()->user());
    }

    public function getUserDocuments()
    {
        return $this->documentInterface->getUserDocuments(auth()->user());
    }

    public function addUserDocument(Request $request)
    {
        $this->documentInterface->addDocumentFile($request, auth()->user());

        return response()->json([
            'message'=>'Upload was successful.'
        ], 200);
    }

    public function updateUserDocument(Request $request)
    {
        $this->documentInterface->updateDocumentFile($request, auth()->user());

        return response()->json([
            'message'=>'Upload was successful.'
        ], 200);
    }

    public function getDocument($document_id)
    {
        return $this->documentInterface->getDocument($document_id);
    }

    public function getUserDocument($user_document_id)
    {
        return $this->documentInterface->getUserDocument($user_document_id);
    }

    public function deleteDocument($document_id)
    {
        return $this->documentInterface->delete($document_id);
    }

    public function deleteDocumentFile($document_id)
    {
        $documents = $this->documentInterface->deleteFile($document_id);

        return response()->json([
            'documents' => $documents
        ]);
    }


}
