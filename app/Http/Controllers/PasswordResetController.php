<?php

namespace App\Http\Controllers;

use App\Models\PasswordReset;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Validator;

class PasswordResetController extends Controller
{
    public function getPasswordReset(Request $request , $token){

        $password_reset = PasswordReset::where('token' , $token)->first();
        $isPast = $password_reset != null ? Carbon::parse($password_reset->expiry_date)->isPast()  : true ;

        if($password_reset == null || $isPast){
            Session::flash('error' , 'Token not valid.');
        }

        $user = User::where('id_number' , $request->id_number)->first();
        $id_number = $user->id_number ?? null;


        return view('password_reset.reset' , compact('id_number' , 'token'));
    }

    public function resetPassword(Request $request){

        $validator = Validator::make($request->all() , [
            'id_number' => 'required|min:13',
            'password' => 'required|confirmed',
        ]);

        if($validator->fails()){
            return redirect()->back()->with('errors' , $validator->errors());
        }

        $password_reset = PasswordReset::where('token' , $request->token)->first();

        if($password_reset !== null && Carbon::parse($password_reset->expiry_date)->isPast() == false){

            User::where('id_number' , $request->id_number)->update([
                'password' => bcrypt($request->password)
            ]);

            Session::flash('success' , 'Please open the app and login with your new password.');
        }
        else{
            Session::flash('error' , 'Token expired');
        }

        return redirect()->back();

    }
}
