<?php

namespace App\Http\Controllers;

use App\Notifications\AccountApproved;
use App\Models\UserDevice;
use Illuminate\Http\Request;

class UserDevicesController extends Controller
{

    public function __construct()
    {

    }

    public function addUserDevice(Request $request){

        return UserDevice::updateOrCreate(
            ['user_id' => $request->userId] , ['device_id' => $request->deviceId]
        );
    }

    public function testNotification($id){

        $user_device = UserDevice::where('user_id' , $id)->first();


        $user_device->notify(new AccountApproved());
    }


}
