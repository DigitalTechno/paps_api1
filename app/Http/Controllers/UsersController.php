<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Repositories\User\UserInterface;
use App\Repositories\UserBudgetItem\UserBudgetItemInterface;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class UsersController extends Controller
{

    private $userBudgetItemInterface;

    private $userInterface;

    public function __construct(
        UserBudgetItemInterface $userBudgetItemInterface,
        UserInterface $userInterface
    ){
        $this->userBudgetItemInterface = $userBudgetItemInterface;
        $this->userInterface = $userInterface;
    }

    public function register(Request $request){

        DB::beginTransaction();

        try{

            //  check if id number is already inside database
            $existing_user = User::where('id_number' , $request->id_number)->first();

            $active = $existing_user != null ? 1 : 0;

            //  password field already used the account already exists
            if($existing_user != null && $existing_user->password != null){
               return response()->json([
                   'message' => config('errors.user_account_exists'),
                   'redirect_url' => 'signin',
               ] , 500);
            }

            //  register user
            $user = User::updateOrCreate([
                'id_number'=> $request->id_number,
            ],[
                'name' => $request->name,
                'surname' => $request->surname,
                'telephone' => "27".substr($request->telephone,1), // patch telephone number with SA dialling code
                'user_type_id' => $request->user_type_id,
                'password' => bcrypt($request->password),
                'active' => $active
            ]);

            $dir = "avatars/{$user->id}";

            if($request->hasFile('file')){

                $fileObject = $request->file('file');

                // upload the file
                $path = $fileObject->store($dir , 'public');

                //  save the file
                $file = File::create([
                   'name' => $fileObject->getClientOriginalName(),
                   'path' => $path,
                   'disk' => 'public',
                   'url' => Storage::disk('public')->url($path)
                ]);

                $user->avatar = $file->id;
                $user->save();
            }

            //  assign fixed items
            $this->userBudgetItemInterface->assignDefaultExpensesToUser($user);


            //  set token if account is active
            if($user->active == 1){
                $token = $this->userInterface->createAuthenticatedToken($user);
            }

            DB::commit();

            return response()->json([
                'user'=> $user,
                'token' => $token ?? null,
                'message' => 'Registration successful.'
            ] , 200);

        }
        catch(\Exception $e){

            DB::rollBack();

            return response()->json([
                'error' => $e->getMessage(),
                'message' => config('errors.general')
            ] , 500);
        }

    }


}
