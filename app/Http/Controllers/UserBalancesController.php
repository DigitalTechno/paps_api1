<?php

namespace App\Http\Controllers;

use App\Repositories\UserBalance\UserBalanceInterface;
use App\Models\UserBalance;
use Illuminate\Http\Request;

class UserBalancesController extends Controller
{

    private $userBalanceInterface;

    public function __construct(UserBalanceInterface $userBalanceInterface)
    {
        $this->middleware('auth:api');

        $this->userBalanceInterface = $userBalanceInterface;
    }


    public function getUserBalance(){

        return $this->userBalanceInterface->getUserBalance(auth()->user());
    }

    /**
     * @return mixed
     */
    public function getNetIncome(){

       return $this->userBalanceInterface->getNetIncome(auth()->user());
    }

}
