<?php

namespace App\Http\Controllers;

use App\Models\UserBudgetItem;
use Illuminate\Http\Request;

class UserBudgetItemsController extends Controller
{
    public function __construct(){

        $this->middleware('auth:api');

    }

    public function updateActiveState(Request $request){

       return  UserBudgetItem::updateOrCreate(
           ['id' => $request->user_budget_item_id]
           ,
       [
           'active' => $request->active
       ]);

    }
}
