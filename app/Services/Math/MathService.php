<?php

namespace App\Services\Math;

class MathService implements MathServiceInterface
{

    public function transformToNegative(float $number)
    {
        return -1 * abs($number);
    }

    public function calculateBalance($net_income, $balance)
    {
        return round($net_income + ($balance) , 2);
    }
}