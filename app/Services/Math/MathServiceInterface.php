<?php

namespace App\Services\Math;

use App\User;

interface MathServiceInterface{

    public function transformToNegative(float $number);

    public function calculateBalance($net_income , $balance);

}