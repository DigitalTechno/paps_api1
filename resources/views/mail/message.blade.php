@component('mail::message')
# Name: {{$user->name .' '.$user->surname}}
Contact Number: {{$user->telephone}}

Message:<br>
<div class="message-container">
  {{$message->message}}
</div>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
`