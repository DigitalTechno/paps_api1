@extends('layout.app')
@section('content')


@if(session('error'))
    <div class="text-center alert alert-danger">
        <strong>Error!</strong> {{ session('error')  }}
    </div>
@endif

@if(session('success'))
    <div class="text-center alert alert-success">
        <strong>Password Reset!</strong> {{ session('success')  }}
    </div>
@endif

<div class="container">
    <div class="row">

        <div class="col-md-4 col-md-offset-4">
            <div class="page-header">
                <h2>Password Reset</h2>
            </div>
            <form method="POST" action="{{ route('password.reset')  }}">

                {{ csrf_field() }}

                <input type="hidden" name="token" value="{{ $token  }}">

                <div class="form-group {{$errors->has('id_number') ? 'has-error' : ''}} ">
                    <label class="control-label" for="">ID Number</label>
                    <input name="id_number" value="{{$id_number}}" readonly type="text" class="form-control">
                    @if($errors->has('id_number'))
                        <div  class="help-block">
                            {{ $errors->get('id_number')[0] }}
                        </div>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                    <label class="control-label" for="">Password</label>
                    <input name="password" type="password" class="form-control">
                    @if($errors->has('password'))
                        <div  class="help-block">
                            {{ $errors->get('password')[0] }}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="">Confirm Password</label>
                    <input name="password_confirmation" type="password" class="form-control">
                </div>

                <button type="submit" class="btn btn-primary">Reset password</button>

            </form>
        </div>
    </div>
</div>

@endsection

