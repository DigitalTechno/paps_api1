<?php

use Illuminate\Database\Seeder;

class DocumentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            0 =>  [
                'title' => 'ID',
                'description' => 'Please load a photo of the back and the front of your ID document'
            ],
            1 => [
                'title' => 'Passport',
                'description' => ''
            ],
            2 => [
                'title' => 'Weapon License',
                'description' => ''
            ],
            3 => [
                'title' => 'Work Visa',
                'description' => ''
            ]
        ];
        foreach ($items as $item){

            \App\Document::updateOrCreate([
                'title' => $item['title'],
                'description' => $item['description'],
                'expires' => 1
            ]);
        }
    }
}
