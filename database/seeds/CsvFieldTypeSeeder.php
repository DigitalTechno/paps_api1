<?php

use Illuminate\Database\Seeder;

class CsvFieldTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            'Information',
            'Earnings',
            'Total Earnings', 
            'Deductions',
            'Total Deductions',
            'Hours',
            'Leave'
        ];

        foreach ($items as $item){

            \App\CsvFieldType::updateOrCreate([
                'title' => $item
            ]);

        }
    }
}
