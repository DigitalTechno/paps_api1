<?php

use Illuminate\Database\Seeder;

class AdvertCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //  truncate data
        \Illuminate\Support\Facades\Schema::disableForeignKeyConstraints();
        \App\AdvertCategory::query()->truncate();
        \Illuminate\Support\Facades\Schema::enableForeignKeyConstraints();

        //  insert fake data
        factory(App\AdvertCategory::class, 50 )->create();
    }
}
