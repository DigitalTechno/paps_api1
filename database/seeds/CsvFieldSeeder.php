<?php

use Illuminate\Database\Seeder;

class CsvFieldSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            0 =>  [
                'title' => 'Name',
                'exclude' => 0,
                'required' => 1,
                'default' => 1,
                'csv_field_type_id' => 1
            ],
            1 =>  [
                'title' => 'Surname',
                'exclude' => 0,
                'required' => 1,
                'default' => 1,
                'csv_field_type_id' => 1
            ],
            2 =>  [
                'title' => 'Employee Code',
                'exclude' => 0,
                'required' => 1,
                'default' => 1,
                'csv_field_type_id' => 1
            ],
            3 =>  [
                'title' => 'ID Number',
                'exclude' => 0,
                'required' => 1,
                'default' => 1,
                'csv_field_type_id' => 1
            ],
            4 =>  [
                'title' => 'Email',
                'exclude' => 0,
                'required' => 0,
                'default' => 1,
                'csv_field_type_id' => 1
            ],
            5 =>  [
                'title' => 'Telephone',
                'exclude' => 0,
                'required' => 0,
                'default' => 1,
                'csv_field_type_id' => 1
            ],
            6 =>  [
                'title' => 'Employed From',
                'exclude' => 0,
                'required' => 1,
                'default' => 1,
                'csv_field_type_id' => 1
            ],
            7 =>  [
                'title' => 'Job Title',
                'exclude' => 0,
                'required' => 1,
                'default' => 1,
                'csv_field_type_id' => 1
            ],

            8 =>  [
                'title' => 'Basic Salary',
                'exclude' => 0,
                'required' => 1,
                'default' => 0,
                'csv_field_type_id' => 2
            ],
            9 =>  [
                'title' => 'Total Earnings',
                'exclude' => 0,
                'required' => 1,
                'default' => 0,
                'csv_field_type_id' => 3
            ],

            10 =>  [
                'title' => 'UIF',
                'exclude' => 0,
                'required' => 1,
                'default' => 0,
                'csv_field_type_id' => 4
            ],
            11 =>  [
                'title' => 'Total Deductions',
                'exclude' => 0,
                'required' => 1,
                'default' => 0,
                'csv_field_type_id' => 5
            ],

            12 =>  [
                'title' => 'Hourly Rate',
                'exclude' => 0,
                'required' => 1,
                'default' => 0,
                'csv_field_type_id' => 6
            ],
            13 =>  [
                'title' => 'Total Hours',
                'exclude' => 0,
                'required' => 1,
                'default' => 0,
                'csv_field_type_id' => 6
            ],
        ];
        foreach ($items as $i => $item){

            \App\CsvField::updateOrCreate([
                'title' => $item['title'],
                'exclude' => $item['exclude'],
                'required' => $item['required'],
                'default' => $item['default'],
                'csv_field_type_id' => $item['csv_field_type_id'],
                'order' => $i
            ]);
        }
    }
}
