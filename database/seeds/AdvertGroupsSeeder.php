<?php

use Illuminate\Database\Seeder;

class AdvertGroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groups = [
            0 =>  [
                'title' => 'All users',
                'type' => 'all'
            ],
            1 => [
                'title' => 'Users with a positive balance.',
                'type' => 'positive'
            ],
            2 => [
                'title' => 'Users with a negative balance',
                'type' => 'negative'
            ]
        ];

        foreach ($groups as $group){

            \App\AdvertGroup::updateOrCreate([
                'type' => $group['type']
            ] , [
                'title' => $group['title']
            ]);
        }


    }
}
