<?php

use Illuminate\Database\Seeder;

class AdvertSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //  truncate data
        \Illuminate\Support\Facades\Schema::disableForeignKeyConstraints();
        \App\Advert::query()->truncate();
        \Illuminate\Support\Facades\Schema::enableForeignKeyConstraints();

        //  insert fake data
        factory(App\Advert::class, 100 )->create();
    }
}
