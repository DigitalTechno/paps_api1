<?php

use Illuminate\Database\Seeder;

class BudgetItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = ['Rent / Bond' , 'Transport' , 'Food' , 'Alcohol' , 'Clothing' , 'Electricity' , 'Airtime / Data'];

        foreach ($items as $item){

            \App\BudgetItem::updateOrCreate([
                'name' => $item,
                'type' => 'expense',
                'fixed' => 1
            ]);

        }
    }
}
