<?php

use Illuminate\Database\Seeder;

class UserTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = ['admin' , 'employee'];

        foreach ($types as $type){
            \App\UserType::updateOrCreate([
                'type' => $type
            ]);
        }
    }
}
