<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTypesSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(BudgetItemSeeder::class);
        $this->call(AdvertGroupsSeeder::class);
        $this->call(DocumentSeeder::class);
        $this->call(CsvFieldTypeSeeder::class);
        $this->call(CsvFieldSeeder::class);
    }
}
