<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

use App\User;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        User::updateOrCreate([
            'user_type_id' => 1,
            'email' => 'admin@phangelagroup.co.za',
            'id_number' => '',
            'password' => bcrypt('admin'),
            'name' => 'Phangela',
            'surname' => 'Admin',
            'telephone' => '000 000 0000',
            'created_at' => $now,
            'updated_at' => $now
        ]);

    }
}
