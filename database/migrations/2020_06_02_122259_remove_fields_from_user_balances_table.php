<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveFieldsFromUserBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_balances', function (Blueprint $table) {
            $table->dropColumn('job_title');
            $table->dropColumn('employment_date');
            $table->dropColumn('gross_income');
            $table->dropColumn('uif');
            $table->dropColumn('yearly_net_income');
            $table->dropColumn('yearly_gross_income');
            $table->dropColumn('yearly_uif');
            $table->dropColumn('leave_days');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_balances', function (Blueprint $table) {
            $table->string('job_title')->nullable();
            $table->string('employment_date')->nullable();
            $table->double('gross_income')->nullable();
            $table->double('uif')->nullable();
            $table->double('yearly_net_income')->nullable();
            $table->double('yearly_gross_income')->nullable();
            $table->double('yearly_uif')->nullable();
            $table->integer('leave_days')->nullable();
        });
    }
}
