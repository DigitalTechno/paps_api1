<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adverts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');

            $table->integer('file_id')->unsigned()->nullable();
            $table->foreign('file_id')
                ->references('id')
                ->on('files')
                ->onDelete('cascade');

            $table->integer('advert_category_id')->unsigned();
            $table->foreign('advert_category_id')
                ->references('id')
                ->on('advert_categories')
                ->onDelete('cascade');

            $table->integer('advert_group_id')->unsigned();

            $table->foreign('advert_group_id')
                ->references('id')
                ->on('advert_groups')
                ->onDelete('cascade');


            $table->text('link')->nullable();

            $table->date('expiry_date');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('adverts' , function (Blueprint $table){

            $table->dropForeign('adverts_file_id_foreign');
            $table->dropForeign('adverts_advert_category_id_foreign');
            $table->dropForeign('adverts_advert_group_id_foreign');

            $table->drop();
        });

        Schema::dropIfExists('adverts');
    }
}
