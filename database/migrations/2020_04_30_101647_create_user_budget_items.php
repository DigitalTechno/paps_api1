<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserBudgetItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_budget_items', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->integer('budget_item_id')->unsigned();
            $table->foreign('budget_item_id')
                ->references('id')
                ->on('budget_items')
                ->onDelete('cascade');



            $table->double('amount' , 10 , 2)->nullable();
            $table->integer('week')->nullable();
            $table->integer('active')->default(1);
            $table->date('date');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_budget_items' , function (Blueprint $table){
            $table->dropForeign('user_budget_items_user_id_foreign');
            $table->dropForeign('user_budget_items_budget_item_id_foreign');
            $table->drop();
        });

    }
}
