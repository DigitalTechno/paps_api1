<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDocumentFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_document_files', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_document_id')->unsigned();
            $table->integer('file_id')->unsigned();

            $table->foreign('user_document_id')
                ->references('id')
                ->on('user_documents');

            $table->foreign('file_id')
                ->references('id')
                ->on('files');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_document_files' , function (Blueprint $table){
            $table->dropForeign('user_document_files_user_document_id_foreign');
            $table->dropForeign('user_document_files_file_id_foreign');
            $table->drop();
        });
    }
}
