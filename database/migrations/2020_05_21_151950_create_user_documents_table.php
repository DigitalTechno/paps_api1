<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_documents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('document_id')->unsigned();
            $table->integer('file_id')->unsigned();

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->foreign('document_id')
                ->references('id')
                ->on('documents');

            $table->foreign('file_id')
                ->references('id')
                ->on('files');

            $table->date('expiry_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_documents' , function (Blueprint $table){
            $table->dropForeign('user_documents_user_id_foreign');
            $table->dropForeign('user_documents_document_id_foreign');
            $table->dropForeign('user_documents_file_id_foreign');
            $table->drop();
        });
    }
}
