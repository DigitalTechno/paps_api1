<?php

use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(\App\Advert::class, function (Faker $faker) {

    $advert_category_id = rand(1,50);
    $advert_group_id = rand(1, 3);


    $year = rand(2019, 2021);
    $month = rand(1, 12);
    $day = rand(1, 28);

    $date = Carbon::create($year,$month ,$day , 0, 0, 0);

    return [
        'title' => $faker->text,
        'link' => $faker->url,
        'expiry_date' => $date->toDateString(),
        'advert_category_id' => $advert_category_id,
        'advert_group_id' => $advert_group_id,

    ];
});
