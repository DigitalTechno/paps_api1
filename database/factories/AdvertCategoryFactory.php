<?php

use Faker\Generator as Faker;

$factory->define(\App\AdvertCategory::class, function (Faker $faker) {
    return [
        'title' => $faker->company
    ];
});
