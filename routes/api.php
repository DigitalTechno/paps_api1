<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * USER ROUTES
 */
Route::post('register' , 'UsersController@register');

/**
 * AUTH ROUTES
 */
Route::post('login' , 'AuthController@login');
Route::get('me' , 'AuthController@me');

/**
 *  BUDGET ROUTES
 */
Route::get('budget/fixed-items/{user_id}' , 'BudgetController@getUserFixedItems');
Route::post('budget/add/item' , 'BudgetController@addItem');
Route::get('budget/income/items' , 'BudgetController@getIncomeItems');
Route::get('budget/expense/items' , 'BudgetController@getExpenseItems');
Route::get('budget/remove/item/{id}' , 'BudgetController@removeItem');
Route::get('budget/items/{id}' , 'BudgetController@getUserBudgetItems');


/**
 * USER BALANCE ROUTES
 */

Route::get('user/balance' , 'UserBalancesController@getUserBalance');
Route::get('user/income' , 'UserBalancesController@getNetIncome');

/**
 * USER BUDGET ITEM ROUTES
 */

Route::post('user/budget/update/state' , 'UserBudgetItemsController@updateActiveState');


/**
 * ADVERT CATEGORY ROUTES
 */

Route::get('advert/categories' , 'AdvertCategoriesController@getCategoriesForUser');

/**
 * ADVERT ROUTES
 */

Route::get('category/{id}/adverts' , 'AdvertsController@getAdvertsForCategory');


/**
 * USER DEVICE ROUTES
 */

Route::post('user/device/add' , 'UserDevicesController@addUserDevice');


/**
 * TEST NOTIFICATIONS
 */

Route::get('notification/{id}' , 'UserDevicesController@testNotification' );


/**
 * VALIDATION ROUTES
 */

Route::post('validate/isRegistered' , 'ValidationController@idNumberExists');

/**
 * PAYSLIP ROUTES
 */

Route::get('payslips' , 'PayslipsController@getPayslips');

/**
 * CONTACT ROUTES
 */
Route::post('message/send' , 'MessageController@send');

/**
 * DOCUMENTS ROUTES
 */

Route::get('documents' , 'DocumentController@getDocuments');
Route::get('user/documents' , 'DocumentController@getUserDocuments');
Route::post('document/add' , 'DocumentController@addUserDocument');
Route::post('document/update' , 'DocumentController@updateUserDocument');
Route::get('document/{document_id}' , 'DocumentController@getDocument');
Route::get('user/document/{user_document_id}' , 'DocumentController@getUserDocument');
Route::get('document/delete/{user_document_id}' , 'DocumentController@deleteDocument');
Route::get('document/file/delete/{file_id}' , 'DocumentController@deleteDocumentFile');




/**
 * PASSWORD RESET ROUTES
 */

Route::post('password/reset/sms' , 'ForgotPasswordController@sendPasswordResetSMS');

/**
 * NOTIFICATIONS ROUTES
 */
Route::get('notifications' , 'NotificationController@getNotifications');
Route::get('notifications/unread' , 'NotificationController@getUnreadNotifications');
Route::get('notifications/read/{id}' , 'NotificationController@markNotificationAsRead');
Route::get('notifications/{id}' , 'NotificationController@getNotification');
Route::get('latest/notifications' , 'NotificationController@getLatestNotifications');
