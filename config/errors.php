<?php

return [
    'general' => 'Oops! Something went wrong.',
    'user_account_exists' => 'This ID Number is already being used. Please Sign In here.',
    'inactive_account' => 'It appears your account is inactive'
];